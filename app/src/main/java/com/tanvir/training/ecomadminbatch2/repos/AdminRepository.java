package com.tanvir.training.ecomadminbatch2.repos;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.tanvir.training.ecomadminbatch2.callbacks.OnCheckAdminListener;

public class AdminRepository {
    private static final String COLLECTION_ADMIN = "Admins";
    private final FirebaseFirestore db = FirebaseFirestore.getInstance();

    public void checkAdmin(String uid, OnCheckAdminListener listener) {
        final boolean status;
        db.collection(COLLECTION_ADMIN)
                .document(uid)
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot>
                                                   task) {
                        listener.onCheckAdmin(task.getResult().exists());
                    }
                });

    }
}
